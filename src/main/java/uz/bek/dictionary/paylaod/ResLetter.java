package uz.bek.dictionary.paylaod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResLetter {
    private Integer id;

    private String nameUz;

    private String nameRu;
}
