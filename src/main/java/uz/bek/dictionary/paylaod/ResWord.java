package uz.bek.dictionary.paylaod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResWord {
    private Integer id;

    private String wordUz;

    private String wordRu;
}
