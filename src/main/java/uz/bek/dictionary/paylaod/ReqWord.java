package uz.bek.dictionary.paylaod;

import lombok.Data;

@Data
public class ReqWord {
    private Integer id;

    private String wordUz;

    private String wordRu;
}
