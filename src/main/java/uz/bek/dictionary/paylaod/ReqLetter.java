package uz.bek.dictionary.paylaod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqLetter {

    private Integer id;

    private String nameUz;

    private String nameRu;

    private boolean flag;


}
