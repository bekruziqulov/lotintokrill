package uz.bek.dictionary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.bek.dictionary.entity.Word;

import java.util.List;
import java.util.Optional;

public interface WordRepository extends JpaRepository<Word, Integer> {
    List<Word> findAllByWordUzContainsIgnoreCaseOrWordRuContainsIgnoreCase(String wordUz, String wordRu);

    @Query(value = "select * from word where word_uz=:wordUz",nativeQuery = true)
    Optional<Word> findByWordUz(String wordUz);

    @Query(value = "select * from word where word_ru=:wordRu",nativeQuery = true)
    Optional<Word> findByWordRu(String wordRu);
}
