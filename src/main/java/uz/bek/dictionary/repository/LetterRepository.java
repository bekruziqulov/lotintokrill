package uz.bek.dictionary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.bek.dictionary.entity.Letter;

import java.util.List;
import java.util.Optional;

public interface LetterRepository extends JpaRepository<Letter, Integer> {
    List<Letter> findAllByNameUzContainsIgnoreCaseOrNameRuContainsIgnoreCaseAndFlagIsTrue(String nameUz, String nameRu);

    @Query(value = "select * from letter where lower(:nameUz)=name_uz", nativeQuery = true)
    Optional<Letter> findByNameUzContainsIgnoreCase(String nameUz);

    @Query(value = "select * from letter where lower(:nameRu)=name_ru and flag = false", nativeQuery = true)
    Optional<Letter> findByNameRuContainsIgnoreCase(String nameRu);

}
