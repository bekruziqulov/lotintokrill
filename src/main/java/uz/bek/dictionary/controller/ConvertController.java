package uz.bek.dictionary.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.bek.dictionary.paylaod.ApiResponseModel;
import uz.bek.dictionary.paylaod.ReqInput;
import uz.bek.dictionary.service.ConvertService;

@RestController
@RequestMapping("/api/convert")
public class ConvertController {

    final
    ConvertService convertService;


    public ConvertController(ConvertService convertService) {
        this.convertService = convertService;
    }

    @PostMapping
    public ApiResponseModel convert(@RequestBody ReqInput reqInput) {
        return convertService.convert(reqInput);
    }
}
