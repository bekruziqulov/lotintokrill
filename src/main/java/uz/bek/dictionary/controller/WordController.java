package uz.bek.dictionary.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.bek.dictionary.paylaod.ApiResponse;
import uz.bek.dictionary.paylaod.ApiResponseModel;
import uz.bek.dictionary.paylaod.ReqWord;
import uz.bek.dictionary.service.WordService;

@RestController
@RequestMapping("/api/word")
public class WordController {
    @Autowired
    WordService wordService;

    @PostMapping
    public ApiResponse saveWord(@RequestBody ReqWord reqWord) {
        return wordService.saveWord(reqWord);
    }

    @DeleteMapping
    public ApiResponse deleteWord(@PathVariable Integer id) {
        return wordService.deleteWord(id);
    }

    @GetMapping
    public ApiResponseModel getWords(@RequestParam(value = "searchName", defaultValue = "") String searchName) {
        return wordService.getWords(searchName);
    }

    @GetMapping("{id}")
    public ApiResponseModel getLetter(@PathVariable Integer id) {
        return wordService.getWord(id);
    }
}
