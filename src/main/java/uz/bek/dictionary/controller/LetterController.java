package uz.bek.dictionary.controller;

import org.springframework.web.bind.annotation.*;
import uz.bek.dictionary.paylaod.ApiResponse;
import uz.bek.dictionary.paylaod.ApiResponseModel;
import uz.bek.dictionary.paylaod.ReqInput;
import uz.bek.dictionary.paylaod.ReqLetter;
import uz.bek.dictionary.service.LetterService;

@RestController
@RequestMapping("/api/letter")
public class LetterController {
    final
    LetterService letterService;

    public LetterController(LetterService letterService) {
        this.letterService = letterService;
    }

    @PostMapping
    public ApiResponse saveLetter(@RequestBody ReqLetter reqLetter) {
        return letterService.saveLetter(reqLetter);
    }

    @DeleteMapping
    public ApiResponse deleteLetter(@PathVariable Integer id) {
        return letterService.deleteLetter(id);
    }

    @GetMapping
    public ApiResponseModel getLetters(@RequestParam(value = "searchName", defaultValue = "") String searchName) {
        return letterService.getLetters(searchName);
    }

    @GetMapping("{id}")
    public ApiResponseModel getLetter(@PathVariable Integer id) {
        return letterService.getLetter(id);
    }


}
