package uz.bek.dictionary.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import uz.bek.dictionary.entity.Letter;
import uz.bek.dictionary.entity.Word;
import uz.bek.dictionary.exception.ResourceNotFoundException;
import uz.bek.dictionary.paylaod.*;
import uz.bek.dictionary.repository.LetterRepository;
import uz.bek.dictionary.repository.WordRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LetterService {

    final
    LetterRepository letterRepository;

    final
    WordRepository wordRepository;

    public LetterService(LetterRepository letterRepository, WordRepository wordRepository) {
        this.letterRepository = letterRepository;
        this.wordRepository = wordRepository;
    }

    public ApiResponse saveLetter(ReqLetter reqLetter) {
        try {
            Letter letter = new Letter();
            if (reqLetter.getId() != null) {
                letter = letterRepository.findById(letter.getId()).orElseThrow(() -> new ResourceNotFoundException("letter not found!", "id", reqLetter.getId()));
            }
            letter.setNameUz(reqLetter.getNameUz());
            letter.setNameRu(reqLetter.getNameRu());
            letterRepository.save(letter);
            return new ApiResponse("Muvoffaqiyatli saqlandi!", true);
        } catch (Exception e) {
            return new ApiResponse("Xatolik yuz berdi!", false);
        }
    }

    public ResLetter getResLetter(Letter letter) {
        return new ResLetter(letter.getId(), letter.getNameUz(), letter.getNameRu());
    }

    public ApiResponse deleteLetter(Integer id) {
        try {
            letterRepository.deleteById(id);
            return new ApiResponse("Muvoffaqiyatli o`chirildi!", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Xatolik!", false);
        }
    }

    public ApiResponseModel getLetters(String searchName) {
        List<Letter> result = letterRepository.findAllByNameUzContainsIgnoreCaseOrNameRuContainsIgnoreCaseAndFlagIsTrue(searchName, searchName);
        return new ApiResponseModel(true, "Letters", result);
    }

    public ApiResponseModel getLetter(Integer id) {
        return new ApiResponseModel(true, "Letter", getResLetter(letterRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Letter not found!", "id", id))));
    }
}
