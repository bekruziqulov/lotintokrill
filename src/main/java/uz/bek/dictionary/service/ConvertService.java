package uz.bek.dictionary.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import uz.bek.dictionary.entity.Letter;
import uz.bek.dictionary.entity.Word;
import uz.bek.dictionary.paylaod.ApiResponseModel;
import uz.bek.dictionary.paylaod.ReqInput;
import uz.bek.dictionary.repository.LetterRepository;
import uz.bek.dictionary.repository.WordRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConvertService {
    final
    LetterRepository letterRepository;

    final
    WordRepository wordRepository;

    public ConvertService(LetterRepository letterRepository, WordRepository wordRepository) {
        this.letterRepository = letterRepository;
        this.wordRepository = wordRepository;
    }

    public ApiResponseModel convert(ReqInput reqInput) {
        try {
            String result = reqInput.getWord().trim().replaceAll("\\s+", " ");
            if (reqInput.isLatin()) {
                if (wordRepository.findByWordUz(result).isPresent()) {
                    return new ApiResponseModel(true, "Javob:", wordRepository.findByWordUz(result).get().getWordRu());
                }
            } else {
                if (wordRepository.findByWordRu(result).isPresent()) {
                    return new ApiResponseModel(true, "Javob:", wordRepository.findByWordRu(result).get().getWordUz());
                }
            }

            if (reqInput.getWord().contains(" ")) {
                String[] splitted = result.split(" ");
                List<String> convertedText = new ArrayList<>();
                String[] split;
                for (String word : splitted) {
                    word = word.replaceAll("G'", "Ғ").replaceAll("g'", "ғ");
                    word = word.replaceAll("ch", "ч").replaceAll("CH", "Ч").replaceAll("Ch", "Ч").replaceAll("cH", "Ч");
                    word = word.replaceAll("sh", "ш").replaceAll("Sh", "Ш").replaceAll("SH", "Ш").replaceAll("sH", "Ш");
                    word = word.replaceAll("O'", "Ў").replaceAll("o'", "ў");
                    word = word.replaceAll("ts", "ц").replaceAll("Ts", "Ц").replaceAll("TS", "Ц");
                    word = word.replaceAll("ye", "е").replaceAll("Ye", "Е").replaceAll("yE", "Е");
                    word = word.replaceAll("yo", "ё").replaceAll("Yo", "Ё").replaceAll("YO", "Ё").replaceAll("yO", "Ё");
                    List<String> converted = new ArrayList<>();
                    split = word.split("");
                    if (reqInput.isLatin()) {
                        for (String str : split) {
                            char c = str.charAt(0);
                            Optional<Letter> find;
                            if (Character.isLowerCase(c)) {
                                String ss = Character.toString(c);
                                find = letterRepository.findByNameUzContainsIgnoreCase(ss);
                                if (find.isPresent()) {
                                    converted.add(find.get().getNameRu().toLowerCase());
                                } else {
                                    converted.add(ss.toLowerCase());
                                }
                            } else {
                                String ss = Character.toString(c);
                                find = letterRepository.findByNameUzContainsIgnoreCase(ss);
                                if (find.isPresent()) {
                                    converted.add(find.get().getNameRu().toUpperCase());
                                } else {
                                    converted.add(ss.toUpperCase());
                                }
                            }
                        }
                    } else {
                        for (String str : split) {
                            char c = str.charAt(0);
                            Optional<Letter> find;
                            if (Character.isLowerCase(c)) {
                                String ss = Character.toString(c);
                                find = letterRepository.findByNameRuContainsIgnoreCase(ss);
                                if (find.isPresent()) {
                                    converted.add(find.get().getNameUz().toLowerCase());
                                } else {
                                    converted.add(ss.toLowerCase());
                                }
                            } else {
                                String ss = Character.toString(c);
                                find = letterRepository.findByNameRuContainsIgnoreCase(ss);
                                if (find.isPresent()) {
                                    converted.add(find.get().getNameUz().toUpperCase());
                                } else {
                                    converted.add(ss.toUpperCase());
                                }
                            }
                        }
                    }
                    convertedText.add(StringUtils.join(converted, ""));
                }
                return new ApiResponseModel(true, "Javob:", StringUtils.join(convertedText, " "));
            } else {
                result = result.replaceAll("G'", "Ғ").replaceAll("g'", "ғ");
                result = result.replaceAll("ch", "ч").replaceAll("CH", "Ч").replaceAll("Ch", "Ч").replaceAll("cH", "Ч");
                result = result.replaceAll("sh", "ш").replaceAll("Sh", "Ш").replaceAll("SH", "Ш").replaceAll("sH", "Ш");
                result = result.replaceAll("ts", "ц").replaceAll("Ts", "Ц").replaceAll("TS", "Ц");
                result = result.replaceAll("O'", "Ў").replaceAll("o'", "ў");
                result = result.replaceAll("ye", "е").replaceAll("Ye", "Е").replaceAll("yE", "Е");
                result = result.replaceAll("yo", "ё").replaceAll("Yo", "Ё").replaceAll("YO", "Ё").replaceAll("yO", "Ё");
                String[] split = result.split("");
                List<String> convertedStr = new ArrayList<>();
                if (reqInput.isLatin()) {
                    for (String str : split) {
                        char c = str.charAt(0);
                        Optional<Letter> find;
                        if (Character.isLowerCase(c)) {
                            String ss = Character.toString(c);
                            find = letterRepository.findByNameUzContainsIgnoreCase(ss);
                            if (find.isPresent()) {
                                convertedStr.add(find.get().getNameRu().toLowerCase());
                            } else {
                                convertedStr.add(ss.toLowerCase());
                            }
                        } else {
                            String ss = Character.toString(c);
                            find = letterRepository.findByNameUzContainsIgnoreCase(ss);
                            if (find.isPresent()) {
                                convertedStr.add(find.get().getNameRu().toUpperCase());
                            } else {
                                convertedStr.add(ss.toUpperCase());
                            }
                        }
                    }
                } else {
                    for (String str : split) {
                        char c = str.charAt(0);
                        Optional<Letter> find;
                        if (Character.isLowerCase(c)) {
                            String ss = Character.toString(c);
                            find = letterRepository.findByNameRuContainsIgnoreCase(ss);
                            if (find.isPresent()) {
                                convertedStr.add(find.get().getNameUz().toLowerCase());
                            } else {
                                convertedStr.add(ss.toLowerCase());
                            }
                        } else {
                            String ss = Character.toString(c);
                            find = letterRepository.findByNameRuContainsIgnoreCase(ss);
                            if (find.isPresent()) {
                                convertedStr.add(find.get().getNameUz().toUpperCase());
                            } else {
                                convertedStr.add(ss.toUpperCase());
                            }
                        }
                    }
                }
                return new ApiResponseModel(true, "Javob:", StringUtils.join(convertedStr, ""));

            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponseModel(false, "Xatolik yuz berdi!");
        }
    }

}
