package uz.bek.dictionary.service;

import org.springframework.stereotype.Service;
import uz.bek.dictionary.entity.Word;
import uz.bek.dictionary.exception.ResourceNotFoundException;
import uz.bek.dictionary.paylaod.ApiResponse;
import uz.bek.dictionary.paylaod.ApiResponseModel;
import uz.bek.dictionary.paylaod.ReqWord;
import uz.bek.dictionary.paylaod.ResWord;
import uz.bek.dictionary.repository.WordRepository;

import java.util.List;

@Service
public class WordService {

    final
    WordRepository wordRepository;

    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public ApiResponse saveWord(ReqWord reqWord) {
        try {
            Word word = new Word();
            if (reqWord.getId() != null) {
                word = wordRepository.findById(word.getId()).orElseThrow(() -> new ResourceNotFoundException("Word not found!", "id", reqWord.getId()));
            }
            word.setWordUz(reqWord.getWordUz());
            word.setWordRu(reqWord.getWordRu());
            wordRepository.save(word);
            return new ApiResponse("Muvoffaqiyatli saqlandi!", true);
        } catch (Exception e) {
            return new ApiResponse("Xatolik yuz berdi!", false);
        }
    }

    public ResWord getResWord(Word word) {
        return new ResWord(word.getId(), word.getWordUz(), word.getWordRu());
    }

    public ApiResponse deleteWord(Integer id) {
        try {
            wordRepository.deleteById(id);
            return new ApiResponse("Muvoffaqiyatli o`chirildi!", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Xatolik!", false);
        }
    }

    public ApiResponseModel getWords(String searchName) {
        List<Word> result = wordRepository.findAllByWordUzContainsIgnoreCaseOrWordRuContainsIgnoreCase(searchName, searchName);
        return new ApiResponseModel(true, "Words", result);
    }

    public ApiResponseModel getWord(Integer id) {
        return new ApiResponseModel(true, "Word", getResWord(wordRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Word not found!", "id", id))));
    }
}
